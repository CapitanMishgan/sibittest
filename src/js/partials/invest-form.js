
var defaultInvestFormData = {
	investAmounts: {
		min: 100,
		max: 200000,
		default: 5000,
		minError: "Минимальная сумма инвестиции ",
		maxError: "Максимальная сумма инвестиции ",
	},
	multiple: {
		min: 1,
		max: 40,
		default: 40,
		minError: "Минимальное значение мультипликатора ",
		maxError: "Максимальное занчение мультипликатора ",
	},
	limits: {
		'%': {
			checkProfit: function(val){
				return val>this.profitMin;
			},
			checkLoss: function(val){
				return val<this.lossMax;
			},
			profitMin: 10,
			profitMax: 1000000,
			lossMin: 0,
			lossMax: 100,
			limitStep: 1,
		},
		'$': {
			checkProfit: function(val){
				return val>this.profitMin;
			},
			checkLoss: function(val){
				return val<this.lossMax;
			},
			profitMin: 1000,
			profitMax: 1000000000,
			lossMin: 0,
			lossMax: 10000,
			limitStep: 100,
		}
	}
}
var data = $.extend(defaultInvestFormData, {
	sumInv: 0,
	sumInvErrorMsg: '',
	sumInvErrorFixVal: 0,
	mult: 0,
	isLimitsShow: false,
	limitType: '%',
	takeProfitCheck: false,
	takeProfitError: false,
	takeProfitVal: 0,
	stopLossCheck: false,
	stopLossError: false,
	stopLossVal: 0,
	takeProfitDefaultVal: 0,
	stopLossDefaultVal: 0,
	isShowMultiSlider: false,
	submitInProcess: false,
});
var investForm = new Vue({
	el: "#invest-form",
	data: data,
	methods: {
		validateParams: function(){
			return (
				!this.sumInvErrorMsg &&
				(!this.takeProfitCheck || !this.stopLossError) &&
				(!this.stopLossCheck || !this.takeProfitError)
			);
		},
		makeInvest: function(direction){
			if(this.validateParams() && !this.submitInProcess){
				var data={};
				data.sumInv=this.sumInv;
				data.mult=this.mult;
				data.direction=direction;
				if(this.takeProfitCheck){
					if(this.limitType==='%'){
						data.takeProfit=(1+(this.takeProfitVal/100))*this.sumInv;
					}else{
						data.takeProfit=this.takeProfitVal;
					}
				}
				if(this.stopLossCheck){
					if(this.limitType==='%'){
						data.stopLoss=(1-(this.stopLossVal/100))*this.sumInv;
					}else{
						data.stopLoss=this.stopLossVal;
					}
					
				}
				this.submitInProcess = true;
				(function(vue){
					$.ajax({
						url: '/php/ajax.php',
						method: 'POST',
						data: data,
						dataType: 'json',
						success: function(){
							alert('Успiх');
						},
						complete: function(){
							vue.submitInProcess = false;
						}
					});
				})(this);
			}
		},
		toggleLimits: function(){
			this.isLimitsShow = !this.isLimitsShow;
		},
		fixInvTo: function(val){
			this.sumInv = val;
		},
		showMultiSlider:function(){
			this.isShowMultiSlider = true;
		},
		hideMultiSlider:function(){
			this.isShowMultiSlider = false;
		},
		changeLimitType: function(type){
			this.takeProfitVal = this.takeProfitDefaultVal = (this.limitType==='%')?30:(this.sumInv*1.3);
			this.stopLossVal = this.stopLossDefaultVal = (this.limitType==='%')?30:(this.sumInv*0.7)
		}
	},
	watch: {
		sumInv: function(val){
			val = parseInt(val, 10) || 0;
			if(val<this.investAmounts.min){
				this.sumInvErrorMsg = this.investAmounts.minError;
				this.sumInvErrorFixVal = this.investAmounts.min;
			}else if(val>this.investAmounts.max){
				this.sumInvErrorMsg = this.investAmounts.maxError;
				this.sumInvErrorFixVal = this.investAmounts.max;
			}else {
				this.sumInvErrorMsg = false;
				this.sumInv = val;
			}
		},
		mult: function(val){
			val = parseInt(val, 10) || 0;
			if(val<this.multiple.min){
				this.mult = this.multiple.min;
			}else if(val>this.multiple.max){
				this.mult = this.multiple.max;
			}else {
				this.mult = val;
			}
		},
		takeProfitVal: function(val){
			val = parseInt(val, 10) || 0;
			this.takeProfitError = !this.limits[this.limitType].checkProfit(val);
		},
		stopLossVal: function(val){
			val = parseInt(val, 10) || 0;
			this.stopLossError = !this.limits[this.limitType].checkLoss(val);
		},
		limitType: function(val){
			this.changeLimitType(val);
		}
	},
	computed: {
		sumFact: function(){
			return this.sumInv*this.mult;
		}
	},
	mounted: function () {
		this.sumInv = this.investAmounts.default;
		this.mult = this.multiple.default;
		this.limitType=Object.keys(this.limits)[0];
		this.changeLimitType(this.limitType);
	},
	components: {
		'vue-slider-component': window['vue-slider-component'],
	},
});