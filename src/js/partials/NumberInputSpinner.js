Vue.component('vnis', {
	data: function () {
		return {
			numericValue: this.value,
		};
	},

	props: {
		value: {
			type: Number,
			default: 0
		},
		min: {
			default: 0,
			type: Number
		},
		max: {
			default: 10,
			type: Number
		},
		step: {
			default: 1,
			type: Number
		},
		inputClass: {
			default: 'form-control vnis__input',
			type: String
		},
		buttonClass: {
			default: 'vnis__button',
			type: String
		},
		integeronly: {
			default: false,
			type: Boolean
		},
		disabled: {
			default: false,
			type: Boolean
		},
	},

	methods: {
		increaseNumber:function() { if(!this.disabled){ this.numericValue += this.step;} },

		decreaseNumber:function() { if(!this.disabled){ this.numericValue -= this.step;} },

		isInteger:function(evt) {
			evt = (evt) ? evt : window.event;
			var key = evt.keyCode || evt.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;

			if( !regex.test(key) ) {
				evt.returnValue = false;
				if(evt.preventDefault) evt.preventDefault();
			}
		},

		isNumber:function(evt) {
		   evt = (evt) ? evt : window.event;
		   var charCode = (evt.which) ? evt.which : evt.keyCode;

		   if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
			   evt.preventDefault();
		   }
		   else {
			   return true;
		   }
	   },

	   validateInput:function(evt) {
		  if ( this.integeronly === true) {
			this.isInteger(evt);
		  }
		  else {
			  this.isNumber(evt);
		  }
	   }
	},

	watch: {
		numericValue: function(val, oldVal){
			val = parseInt(val, 10);
			oldVal = parseInt(oldVal, 10);
			if( val <= this.min ) { this.numericValue = parseInt(this.min); }

			if( val >= this.max ) { this.numericValue = parseInt(this.max); }

			if( val <= this.max && val >= this.min ) {
				this.$emit('input', val, oldVal );
			}
		}
	},
	template: '<span class="vnis" :disabled="disabled">'+
		'<button @click.prevent="decreaseNumber" class="vnis__button_down" :class="buttonClass"></button>'+
		'<input '+
			'type="number" '+
			':disabled="disabled" '+
			':value="value" '+
			'@keypress="validateInput" '+
			'@input="numericValue = $event.target.value" '+
			':class="inputClass" '+
			':min="min" '+
			':max="max" '+
		'/>'+
		'<button @click.prevent="increaseNumber" class="vnis__button_up" :class="buttonClass"></button>'+
	  '</span>'
})