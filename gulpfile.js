'use strict';

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();
var browserSync = require("browser-sync");
var reload = browserSync.reload;
var prefixer = require('gulp-autoprefixer');
var cssclean = require('gulp-clean-css');
var pngquant = require('imagemin-pngquant');
var rimraf = require('rimraf');


var paths = {
    build: { 
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
//        fonts: 'build/fonts/'
    },
    src: { 
        html: 'src/*.html',
        js: 'src/js/main.js',
        css: 'src/scss/style.scss',
        img: 'src/img/**/*.*',
//        fonts: 'src/fonts/**/*.*'
    },
    watch: { 
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        css: 'src/scss/**/*.scss',
        img: 'src/img/**/*.*',
//        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'sibittest.local',
    port: 9991,
    logPrefix: "cm"
};


gulp.task('html:build', function () {
    gulp.src(paths.src.html) //Выберем файлы по нужному пути
        .pipe(plugins.rigger()) //Прогоним через rigger
        .pipe(gulp.dest(paths.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});
gulp.task('js:build', function () {
    gulp.src(paths.src.js) //Найдем наш main файл
        .pipe(plugins.rigger()) //Прогоним через rigger
        .pipe(plugins.sourcemaps.init()) //Инициализируем sourcemap
        .pipe(plugins.uglify()) //Сожмем наш js
        .pipe(plugins.sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(paths.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});
gulp.task('css:build', function () {
    gulp.src(paths.src.css) //Выберем наш main.scss
        .pipe(plugins.sourcemaps.init()) //То же самое что и с js
        .pipe(plugins.sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssclean()) //Сожмем
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(paths.build.css)) //И в build
        .pipe(reload({stream: true}));
});
gulp.task('image:build', function () {
    gulp.src(paths.src.img) //Выберем наши картинки
        .pipe(plugins.imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(paths.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'css:build',
    'image:build',
]);
gulp.task('watch', function(){
    plugins.watch([paths.watch.html], function() {
        gulp.start('html:build');
    });
    plugins.watch(paths.watch.css, function() {
        gulp.start('css:build');
    });
    plugins.watch([paths.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    plugins.watch([paths.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});
gulp.task('clean', function (cb) {
    rimraf(paths.clean, cb);
});
gulp.task('default', ['build', 'webserver', 'watch']);